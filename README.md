# PUISSANCE4
##notes
js add class to an element:
td.className ='COLUMN'+j;

js add id to an element:
tr.id = 'LIGNE'+i;

js access background color of an element:
td.style.backgroundColor = "red";


##consigns
Créer une variable Javascript contenant un tableau en deux dimensions de 7 par 6.
Créer dans la page HTML une balise 'TABLE id="jeu"' vide.
Créer une fonction qui prend en entrée le tableau de données précédemment créé, et qui renvoye le nombre de cases pleines. Par ailleurs, elle génère les balises TR et TD en fonction des règles suivantes :


la valeur est de type INT = jaune
la valeur est de type STRING = rouge
la valeur est d'un autre type = case vide
les balises TR possèdent un ID nommé LIGNE[numéro de ligne]
les balises TD possèdent une classe nommée COLONNE[numéro de colonne]


Créer une fonction qui vide le contenu de la base 'TABLE id="jeu"', et qui renvoie le nombre de caractères supprimés.
Créer une fonction qui prend en entrée 4 valeurs : un tableau A à deux dimensions, une abscisse X, une ordonnée Y, une valeur V. Cette fonction renvoie un tableau A mis à jour de telle sorte que la valeur aux coordonnées [X,Y] est égale à V.
Créer une fonction qui prend en entrée un nombre de 0 à 6, et qui renvoie le nombre de valeurs non nulles de cette colonne donnée.
Créer un bouton HTML qui - au clic - met à jour l'affichage, par l'appel successif des fonctions créées aux étapes 4 et 3.
Ajouter un événement sur chaque cellule TD, qui affiche dans le terminal du navigateur le numéro de colonne cliqué (pour rappel, la cellule cliquée doit posséder une classe nommée COLONNE[numéro de colonne])
Créer une fonction qui prend trois arguments : le tableau à 2 dimensions T, un numéro de colonne C, et une valeur V (numérique ou texte). La valeur V doit être insérée à la première place vide de la colonne C.


